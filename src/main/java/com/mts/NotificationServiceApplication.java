package com.mts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotificationServiceApplication {
	
	//private static final String EXCHANGE_NAME = "pub-sub-queue";
	/*
	 * //@Value("${spring.rabbitmq.host}") private static String host="localhost";
	 * 
	 * //@Value("${rabbitmq.queue}") private static String queueName="user-queue";
	 */
	public static void main(String[] args) throws Exception {
		SpringApplication.run(NotificationServiceApplication.class, args);
		/*
		 * final ConnectionFactory connectionFactory = new ConnectionFactory();
		 * connectionFactory.setHost(host);
		 * 
		 * final Connection connection = connectionFactory.newConnection(); final
		 * Channel channel = connection.createChannel(); channel.queueDeclare(queueName,
		 * false, false, false, null);
		 * 
		 * System.out.
		 * println("Waiting for messages from the queue. To exit press CTRL+C");
		 * 
		 * final DeliverCallback deliverCallback = (consumerTag, delivery) -> { final
		 * String message = new String(delivery.getBody(), "UTF-8");
		 * System.out.println("Received from message from the queue: " + message); };
		 * 
		 * channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {});
		 */
		
		
		
		
		/*
		 * ConnectionFactory factory = new ConnectionFactory();
		 * factory.setHost("localhost"); Connection connection =
		 * factory.newConnection(); Channel channel = connection.createChannel();
		 * 
		 * channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT); String
		 * queueName = channel.queueDeclare().getQueue(); channel.queueBind(queueName,
		 * EXCHANGE_NAME, "");
		 * 
		 * System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
		 * 
		 * DeliverCallback deliverCallback = (consumerTag, delivery) -> { String message
		 * = new String(delivery.getBody(), "UTF-8");
		 * System.out.println(" [x] Received '" + message + "'"); };
		 * channel.basicConsume(queueName, true, deliverCallback, consumerTag -> { });
		 */
		
	}

}
