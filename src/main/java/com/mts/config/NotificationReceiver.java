package com.mts.config;

import org.apache.log4j.Appender;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;


@Configuration
public class NotificationReceiver {
	
	private static Logger log=Logger.getLogger(NotificationReceiver.class);
	Layout layout=new SimpleLayout();
	Appender ap=new ConsoleAppender(layout);
	
	//private static final String LISTENER_METHOD = "receiveMessage";
	
	
	/*
	 * @Autowired Queue queue;
	 */	
	
	
	//@Value("${spring.rabbitmq.template.default-receive-queue}")
	@Value("${rabbitmq.queue}")
	String queueName;
	
	@Value("${topic.exchange}")
	 private String exchange;
	
	
	
	/*
	 * @Bean Queue queue() { return new Queue(queueName, true); }
	 * 
	 * @Bean FanoutExchange exchange() { return new FanoutExchange(fanoutExchange);
	 * }
	 * 
	 * @Bean Binding binding(Queue queue, FanoutExchange exchange) { return
	 * BindingBuilder.bind(queue).to(exchange); }
	 * 
	 * @Bean SimpleMessageListenerContainer container(ConnectionFactory
	 * connectionFactory, MessageListenerAdapter listenerAdapter) {
	 * SimpleMessageListenerContainer container = new
	 * SimpleMessageListenerContainer();
	 * container.setConnectionFactory((org.springframework.amqp.rabbit.connection.
	 * ConnectionFactory) connectionFactory); container.setQueueNames(queueName);
	 * container.setMessageListener(listenerAdapter); return container; }
	 * 
	 * @Bean MessageListenerAdapter listenerAdapter(NotificationReceiver
	 * receivedMessage) { return new MessageListenerAdapter(receivedMessage,
	 * LISTENER_METHOD); }
	 */
	@RabbitListener(queues = "${rabbitmq.queue}")
	public void receive(String queueName)throws AmqpException {
		log.addAppender(ap);
		log.info("Notification Received Message");
		System.out.println("Message Recieved"+queueName);
		
	}
	
	
	
}
